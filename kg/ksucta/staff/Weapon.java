package kg.ksucta.staff;

public class Weapon implements Item {
	
	private int damage;
	private String description;
	private int weight;
	private String weaponName;
	public Integer getWeaponDamage()
	{
		return damage;
	}
	@Override
	public String getItemDescription() {

		return description;
	}
	@Override
	public Integer getItemWeight() {
		return weight;
	}
	@Override
	public String getItemName() {
		// TODO Auto-generated method stub
		return this.weaponName;
	}
}
