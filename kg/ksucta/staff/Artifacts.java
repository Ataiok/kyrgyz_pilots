package kg.ksucta.staff;

public class Artifacts implements Item {

	private String description;
	private String name;
	private int weight;
	public Artifacts(String name,String description)
	{
		this.name=name;
		this.description=description;
	}
	@Override
	public String getItemDescription() {
		return description;
		
	}
	@Override
	public Integer getItemWeight() {
		return weight;
	}
	@Override
	public String getItemName() {
		// TODO Auto-generated method stub
		return this.name;
	}

}
