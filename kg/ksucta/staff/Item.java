package kg.ksucta.staff;


public interface Item {

	String getItemDescription();
	
	Integer getItemWeight();
	
	String getItemName();
}
