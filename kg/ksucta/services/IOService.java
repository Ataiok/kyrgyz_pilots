package kg.ksucta.services;


public interface IOService {

    void write(String message);

    String scan(String message);

    Integer scanInt(String message);
}
