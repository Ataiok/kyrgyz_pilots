package kg.kgusta.unit;

import kg.ksucta.staff.Room;

public class Monster implements Unit{

	private Unit underAttackUnit;
	private int health;
	private int monsterDamagePower;
    private String name;
	private Room previousRoom;
	public Monster(int health,int damage,String name)
	{
		this.health=health;
		this.monsterDamagePower=damage;
		this.name=name;
	}
	public void attack() {
		underAttackUnit.harmUnit(monsterDamagePower);
	}
      	@Override
	public Integer getHealth() {
		return health;
	}
		@Override
	public void harmUnit(int damage) {
			this.health-=damage;
	}	
		
	public String getUnitName()
	{
		return this.name;
	}
	@Override
	public void setUnderAttackUnit(Unit unit)
	{
		this.underAttackUnit=unit;
	}
	@Override
	public Room getPreviousRoom() {
		// TODO Auto-generated method stub
		return previousRoom;
	}
}
