package kg.kgusta.unit;

import kg.ksucta.staff.Room;

public interface Unit {

    void attack();

    Integer getHealth();

    void harmUnit(int newHealth);
    
    void setUnderAttackUnit(Unit unit);
    
    String getUnitName();
    
    Room getPreviousRoom();
}
