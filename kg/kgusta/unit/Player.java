package kg.kgusta.unit;

import java.util.List;

import kg.ksucta.staff.Item;
import kg.ksucta.staff.Room;
import kg.ksucta.staff.Weapon;

public class Player implements Unit {
    
	private Unit underAttackUnit;
    private List<Item> items;
    private int health;
    private Item itemOnHand;
    private int maxItemCarryPower;
    private String name;
    private Room previousRoom;
    
    public Player(String name)
    {
    	health=1000;
    	itemOnHand=new Weapon();
    	this.name=name;
    }
    @Override
    public void attack() {
        evaluateWeaponForUnit();
        underAttackUnit.harmUnit(((Weapon) itemOnHand).getWeaponDamage());
        
    }
    private void setPassWeapon(Integer health){

        for (Item item : items) {
                   if(item instanceof Weapon)
                   {
                
                   }
        }        
    }

    private void evaluateWeaponForUnit(){
        Integer health = underAttackUnit.getHealth();
        setPassWeapon(health);
    }

    @Override
    public Integer getHealth() {
        return health;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

	@Override
	public void harmUnit(int damage) {
		
		this.health-=damage;
	}
	public Integer getItemCarryPower()
	{
		return maxItemCarryPower;
	}
	@Override
	public void setUnderAttackUnit(Unit unit) {
     this.underAttackUnit=unit;		
	}
	@Override
	public String getUnitName() {
		return this.name;
	}
	@Override
	public Room getPreviousRoom() {
		// TODO Auto-generated method stub
		return previousRoom;
	}
	public void throwItem(Item item)
	{
		items.remove(item);
	}
}
