package kg.kgusta.gameloop;

import java.util.Random;

public class RandomNames {

	private static String[] commands ={"go","get","pick","attack","throw"};
	private static String[] roomNames ={""};
	private static String[] weaponNames={""};
	private static String[] artifactNames={""};
	private int[] forRoomName=new int[roomNames.length];
	private int[] forWeaponName=new int[weaponNames.length];
	private int[] forArtifactName=new int[artifactNames.length];
	private static Random random;
	private static RandomNames singleClass=new RandomNames();
	
	private RandomNames()
	{
	   random =new Random();
	}
	public static RandomNames getInstance()
	{
		return singleClass;
	}
	public String getRandomWeaponName()
	{
		int oval = chechForNumberRepeatness(forWeaponName);
		return weaponNames[oval];
	}
	public String getRandomArtifactName()
	{
		int oval = chechForNumberRepeatness(forArtifactName);
		return artifactNames[oval];
	}
	public String getRandomRoomName()
	{
		int oval = chechForNumberRepeatness(forRoomName);
		return roomNames[oval];
	}
	private int chechForNumberRepeatness(int[] whichMassive)
	{
		int rand =random.nextInt(whichMassive.length);
		int local=0;
		for(int i =0;i<whichMassive.length;i++)
		{
	       		if(rand==whichMassive[i])
	       		{
	       			local++;
	       		}
		}
		if(0!=local)
			return chechForNumberRepeatness(whichMassive);
		if(0==local)
			whichMassive[whichMassive.length]=rand;
			return rand;
	}
	public boolean commandExists(String com)
	{
		 for(String s:commands)
		 {
			 if(com.equals(s))
			 {
				 return true;
			 }
		 }
		 return false;
 	}
	public String[] getCommandList()
	{
		return commands;
	}
}
